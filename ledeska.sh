cd /root
yum -y install wget
yum -y install psmisc
yum -y install net-tools
yum -y install systemd-devel
yum -y install libdb-devel
yum -y install perl-DBI
yum -y install xfsprogs
yum -y install rsyslog
yum -y install logrotate
yum -y install crontabs
yum -y install file
yum -y install git
yum -y install zip
yum -y install unzip
yum -y update

wget https://bitbucket.org/xledeska/config/get/3d440c4afd20.zip
wget https://bitbucket.org/xledeska/skins/get/c7cf72376fbc.zip
wget https://bitbucket.org/xledeska/templates/get/dfa752f256f1.zip
wget https://bitbucket.org/xledeska/panel/get/6b3f153049c4.zip
wget http://www.directadmin.com/setup.sh

unzip 3d440c4afd20.zip
unzip c7cf72376fbc.zip
unzip dfa752f256f1.zip
unzip 6b3f153049c4.zip

rm -f 3d440c4afd20.zip
rm -f c7cf72376fbc.zip
rm -f dfa752f256f1.zip
rm -f 6b3f153049c4.zip

mv -f xledeska-config-3d440c4afd20 config
mv -f xledeska-skins-c7cf72376fbc skins
mv -f xledeska-templates-dfa752f256f1 templates
mv -f xledeska-panel-6b3f153049c4 panel

mv -f /root/config/resolv.conf /etc/resolv.conf
mv -f /root/config/motd /etc/motd

chmod 755 setup.sh
./setup.sh

cd /usr/local/directadmin/custombuild
./build set eximconf yes
./build set blockcracking yes
./build set easy_spam_fighter yes
./build set exim yes
./build set spamassassin yes
./build set webserver nginx_apache
./build set opcache yes
./build set php1_release 5.6
./build set mariadb 10.0
./build set roundcube yes
./build set squirrelmail no
./build set afterlogic no
./build set awstats yes
./build set webalizer yes
./build set clamav yes
./build set suhosin yes
./build all d
./build update
./build rewrite_confs

rm -R -f /var/www/html/roundcube
rm -R -f /var/www/html/roundcubemail-*
rm -R -f /var/www/html/squirrelmail
rm -R -f /var/www/html/squirrelmail-*
rm -R -f /var/www/html/afterlogic
rm -R -f /var/www/html/webmail

./build roundcube
./build update
./build rewrite_confs

rm -R -f /var/www/html/roundcube
mv -f /var/www/html/roundcubemail-* /var/www/html/webmail

cd /root

rm -R -f /var/www/html/webmail/skins/classic
rm -R -f /var/www/html/webmail/skins/larry
rm -R -f /usr/local/directadmin/data/templates/default
rm -R -f /usr/local/directadmin/data/templates/sharedip
rm -R -f /usr/local/directadmin/data/templates/suspended
rm -R -f index.html
rm -R -f redirect.php

rm -R -f /usr/local/directadmin/data/skins/enhanced
rm -R -f /usr/local/directadmin/data/skins/power_user

mv -f /root/skins/desktop /var/www/html/webmail/skins/larry
mv -f /root/skins/mobile /var/www/html/webmail/skins/mobile
mv -f /root/templates/server/* /var/www/html/

mv -f /root/skins/hosting /usr/local/directadmin/data/skins/hosting

mv -f -f /root/templates/default /usr/local/directadmin/data/templates/default
rm -R -f /root/templates/sharedip /usr/local/directadmin/data/templates/sharedip
rm -R -f /root/templates/suspended /usr/local/directadmin/data/templates/suspended

mv -f /root/config/edit.php /var/www/html/webmail/config/edit.php
mv -f /root/config/webapps.conf /etc/nginx/webapps.conf
mv -f /root/config/webapps.ssl.conf /etc/nginx/webapps.ssl.conf
mv -f /root/config/custom/dns_a.conf /usr/local/directadmin/data/templates/custom/dns_a.conf
mv -f /root/config/nginx_server.conf /usr/local/directadmin/data/templates/custom/nginx_server.conf
mv -f /root/config/virtual_host.conf /usr/local/directadmin/data/templates/custom/virtual_host.conf

rm -R -f config
rm -R -f skins
rm -R -f templates
rm -R -f panel
rm -R -f setup.sh

passwd admin

reboot